require! {
	request
	colors
}

errors = do
	name: "Error! Invalid city name!".red
	request: "Couldn't connect to server".red
	city: "Please, enter a city".red

success = do
	connect: "Connecting to service, please, wait...\n".green


get-weather = (city-name, cb) ->
	city-name = city-name + ','
	url = "http://api.openweathermap.org/data/2.5/weather?q=#{city-name}&units=metric&lang=en"

	(err, res, body) <- request url
	return cb errors.request, null if err?

	(err, weather) <- parse-weather res.body
	return cb err, null if err?
	cb null, weather


parse-weather = (json, cb) ->
	data = JSON.parse json
	return cb errors.name, null if data.cod in <[404 500]> or data.name is ''

	directions = <[ north north-east east south-east south south-west west north-west ]>
	plus = if data.main.temp > 0 then '+' else ''
	mmHg-coefficient = 7.50061683

	cb null, do
		temp: plus + Math.round data.main.temp.to-string!
		wind: Math.round data.wind.speed .to-string!
		direction: directions[( Math.round data.wind.deg / 45)-1]
		humidity: data.main.humidity.to-string!
		pressure: data.main.pressure |>  (/ 10) |> (* mmHg-coefficient) |> Math.round
		weather: data.weather.0.description
		name: "#{data.name}, #{data.sys.country}"


exit = (err)->
	console.error err
	process.exit 1


run = ->
	return exit errors.city if not process.argv.2?
	process.stdout.write success.connect

	(err, result) <- get-weather process.argv.2
	return exit err if err?

	date = new Date().to-string!
	template = "" +
		"\tWeather in #{result.name} at #{date.substr 0, date.length-15 .magenta} \n\n".yellow +
		"\tTemperature: #{result.temp.green} \n".blue +
		"\tWind speed: #{result.wind.green} m/s, #{result.direction.green} \n".blue +
		"\tWeather: #{result.weather.green} \n".blue +
		"\tHumidity: #{result.humidity.green}#{'%'.green}\n".blue +
		"\tPressure: #{result.pressure.to-string!.green} mmHg\n\n".blue

	process.stdout.write template

run!
